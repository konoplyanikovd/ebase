<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function __invoke(Request $request)
    {

        return redirect('/users');
    }
}
