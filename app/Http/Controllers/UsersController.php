<?php

namespace App\Http\Controllers;

use App\Models\DisabledCategory;
use App\Models\Education;
use App\Models\MaritalStatus;
use App\Models\User;
use App\Models\WorkStatus;
use App\Models\WorkType;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $usersQuery = User::with(['user_addresses', 'user_categories', 'work_type', 'marital_status', 'work_status']); 
        
        $users = $usersQuery->get();
        
        $json = $request->json()->all();

        $disabledCategories = DisabledCategory::all();
        $workTypes = WorkType::all();
        $workStatuses = WorkStatus::all();
        $maritalStatuses = MaritalStatus::all();
        $educations = Education::all();

        if (count($json) > 0) {
            return view('users.table')
                ->with('users', $users)
                ;
        }

        return view('users.index')
            ->with('request', $request)
            ->with('users', $users)
            ->with('disabledCategories', $disabledCategories)
            ->with('workStatuses', $workStatuses)
            ->with('maritalStatuses', $maritalStatuses)
            ->with('educations', $educations)
            ->with('workTypes', $workTypes)
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
