<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table = 'addresses';

    protected $fillable = [
        'region',
        'city',
        'street',
        'building',
        'region_id',
        'city_id',
        'street_id',
        'building_id',
    ];
}
