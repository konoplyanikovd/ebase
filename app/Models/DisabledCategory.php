<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisabledCategory extends Model
{
    use HasFactory;

    protected $table = 'disabled_categories';

    protected $fillable = [
        'ru_name',
        'kk_name',
        'code',
    ];
}
