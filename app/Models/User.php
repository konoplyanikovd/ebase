<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'last_name',
        'middle_name',
        'iin',
        'birthday',
        'mobile_phone',
        'marital_status_id',
        'family_units',
        'disable_group',
        'education_id',
        'specialty',
        'work_experience_common',
        'work_experience_by_specialty',
        'prev_work_place_name',
        'current_work_place_name',
        'current_work_place_position',
        'work_status_id',
        'work_type_id',
        'needle_help',
        'some_note',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'needle_help' => 'array',
        'some_note' => 'array',
    ];

    protected $attributes = [
        'needle_help' => [],
        'some_note' => [],
    ];

    
    public function getFioAttribute()
    {
        return "{$this->last_name} {$this->name} {$this->middle_name}";
    }
    
    public function getAgeAttribute()
    {
        return Carbon::parse($this->birthday)->age;
    }

    public function user_addresses() 
    {
        return $this->hasMany(\App\Models\UserAddress::class);
    }
    
    public function user_categories() 
    {
        return $this->hasMany(\App\Models\UserDisabledCategory::class);
    }
    
    public function marital_status() 
    {
        return $this->hasOne('App\Models\MaritalStatus', 'id', 'marital_status_id');
    }
    
    public function work_status() 
    {
        return $this->hasOne('App\Models\WorkStatus', 'id', 'work_status_id');
    }
    
    public function work_type() 
    {
        return $this->hasOne('App\Models\WorkType', 'id', 'work_type_id');
    }
}
