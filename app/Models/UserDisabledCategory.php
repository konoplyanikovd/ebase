<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDisabledCategory extends Model
{
    use HasFactory;

    protected $table = 'user_disabled_categories';

    protected $fillable = [
        'user_id',
        'disabled_category_id',
    ];
}
