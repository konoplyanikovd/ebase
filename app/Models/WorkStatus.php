<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkStatus extends Model
{
    use HasFactory;

    protected $table = 'work_statuses';

    protected $fillable = [
        'ru_name',
        'kk_name',
        'code',
    ];
}
