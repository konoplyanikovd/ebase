<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last_name')->default('');
            $table->string('middle_name')->default('');
            $table->string('iin', 12)->nullable();
            $table->timestamp('birthday')->nullable();
            $table->unsignedBigInteger('mobile_phone');
            $table->foreignId('marital_status_id')->default(1);
            $table->unsignedTinyInteger('family_units')->default(1)->comment('Кол-во проживающих в семье');
            $table->unsignedTinyInteger('disable_group')->nullable()->comment('Группа инвалидности');
            $table->foreignId('education_id')->default(4);
            $table->string('specialty')->nullable()->comment('Специальность (по диплому)');
            $table->string('work_experience_common')->nullable()->comment('общий стаж работы');
            $table->string('work_experience_by_specialty')->nullable()->comment('стаж по специальности');
            $table->string('prev_work_place_name')->nullable();
            $table->string('current_work_place_name')->nullable();
            $table->string('current_work_place_position')->nullable();
            $table->foreignId('work_status_id')->default(2);
            $table->foreignId('work_type_id')->nullable();
            $table->json('needle_help');
            $table->json('some_note');
            
            $table->foreign('marital_status_id')->references('id')->on('marital_statuses');
            $table->foreign('education_id')->references('id')->on('education');
            $table->foreign('work_status_id')->references('id')->on('work_statuses');
            $table->foreign('work_type_id')->references('id')->on('work_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['marital_status_id']);
            $table->dropForeign(['education_id']);
            $table->dropForeign(['work_status_id']);
            $table->dropForeign(['work_type_id']);
                
            $table->dropColumn([
                'last_name',
                'middle_name',
                'iin',
                'birthday',
                'mobile_phone',
                'marital_status_id',
                'family_units',
                'disable_group',
                'education_id',
                'specialty',
                'work_experience_common',
                'work_experience_by_specialty',
                'prev_work_place_name',
                'current_work_place_name',
                'current_work_place_position',
                'work_status_id',
                'work_type_id',
                'needle_help',
                'some_note',
            ]);
        });
        Schema::enableForeignKeyConstraints();
    }
}
