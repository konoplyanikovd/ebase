<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Education::insert([
            [
                'ru_name' => 'Высшее',
            ],
            [
                'ru_name' => 'Среднее общее',
            ],
            [
                'ru_name' => 'Общее',
            ],
            [
                'ru_name' => 'Основное общее',
            ],
        ]);
    }
}
