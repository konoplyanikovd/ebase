<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MaritalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\MaritalStatus::insert([
            [
                'ru_name' => 'Женат/Замужем',
            ],
            [
                'ru_name' => 'Холост/не замужем',
            ],
            [
                'ru_name' => 'Разведен/Разведена',
            ],
            [
                'ru_name' => 'Вдовец/Вдова',
            ],
        ]);
    }
}
