<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WorkStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\WorkStatus::insert([
            [
                'ru_name' => 'Работает',
            ],
            [
                'ru_name' => 'Не работает',
            ],
        ]);
    }
}
