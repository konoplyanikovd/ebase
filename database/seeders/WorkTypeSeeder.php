<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WorkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\WorkType::insert([
            [
                'ru_name' => 'Постоянная',
            ],
            [
                'ru_name' => 'Временная',
            ],
            [
                'ru_name' => 'Сезонная',
            ],
            [
                'ru_name' => 'Вахтовый метод',
            ],
        ]);
    }
}
