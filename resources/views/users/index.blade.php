@extends('layouts.main')

@section('title', 'Users')

@section('content')

<div class="section">
    <div class="columns">
        <div class="column is-4">
            <label class="label" for="filters[iin]">ИИН</label>
            <div class="field">
                <div class="control">
                    <input class="input" name="filters[iin]" type="text" placeholder="ИИН">
                </div>
            </div>
        </div>
        <div class="column is-4">
            <label class="label" for="filters[disabled_category]">Категория</label>
            <div class="field">
                <div class="control">
                    <div class="select">
                        <select name="filters[disabled_category]">
                            @foreach ($disabledCategories as $category)
                                <option value="{{ $category->id }}">{{ $category->ru_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="column is-4">
            <label class="label">Инвалидность</label>
            <div class="field">
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[disabled_group][]" value="1">
                        <span class="check is-info"></span>
                        <span class="control-label">1 Группа</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[disabled_group][]" value="2">
                        <span class="check is-info"></span>
                        <span class="control-label">2 Группа</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[disabled_group][]" value="3">
                        <span class="check is-info"></span>
                        <span class="control-label">3 Группа</span>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="columns">
        <div class="column">
            <h5 class="title is-4">Адрес</h5>
        </div>
    </div>
    <div class="columns">
        <div class="column is-2">
            <label class="label" for="filters[address][city]">Город</label>
            <div class="field">
                <div class="control">
                    <input class="input" name="filters[address][city]" type="text" placeholder="Город">
                </div>
            </div>
        </div>
        <div class="column is-2">
            <label class="label" for="filters[address][district]">Район</label>
            <div class="field">
                <div class="control">
                    <input class="input" name="filters[address][district]" type="text" placeholder="Район">
                </div>
            </div>
        </div>
        <div class="column is-2">
            <label class="label" for="filters[address][street]">Улица</label>
            <div class="field">
                <div class="control">
                    <input class="input" name="filters[address][street]" type="text" placeholder="Улица">
                </div>
            </div>
        </div>
    </div>
    <div class="columns">
        <div class="column is-4">
            <label class="label" for="filters[marital_status]">Семейное положение</label>
            <div class="field">
                @foreach ($maritalStatuses as $status)
                    <div class="control">
                        <label class="b-checkbox checkbox is-small">
                            <input type="checkbox" name="filters[marital_status][]" value="{{ $status->id }}">
                            <span class="check is-info"></span>
                            <span class="control-label">{{ $status->ru_name }}</span>
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="column is-4">
            <label class="label" for="filters[education]">Образование</label>
            <div class="field">
                @foreach ($educations as $education)
                    <div class="control">
                        <label class="b-checkbox checkbox is-small">
                            <input type="checkbox" name="filters[education][]" value="{{ $education->id }}">
                            <span class="check is-info"></span>
                            <span class="control-label">{{ $education->ru_name }}</span>
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="column is-4">
            <label class="label">Возраст</label>
            <div class="field">
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[ages][]" value="=<20">
                        <span class="check is-info"></span>
                        <span class="control-label">До 20 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[ages][]" value=">=21|<=29">
                        <span class="check is-info"></span>
                        <span class="control-label">От 21 до 29 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[ages][]" value=">=30|<=45">
                        <span class="check is-info"></span>
                        <span class="control-label">От 30 до 45 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[ages][]" value=">=46|<=58">
                        <span class="check is-info"></span>
                        <span class="control-label">От 46 до 58 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[ages][]" value=">=59|<=70">
                        <span class="check is-info"></span>
                        <span class="control-label">От 59 до 70 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox">
                        <span class="check is-info"></span>
                        <span class="control-label">Произвольный период</span>
                    </label>
                    <div class="field is-grouped">
                        <div class="control">
                            <input class="input is-small" type="number" min="1" max="100" name="filters[ages][other][from]" placeholder="От">
                        </div>
                        <div class="control">
                            <input class="input is-small" type="number" min="1" max="100" name="filters[ages][other][to]" placeholder="До">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="columns">
        <div class="column is-4">
            <label class="label" for="filters[work_status]">Статус</label>
            <div class="field">
                @foreach ($workStatuses as $status)
                    <div class="control">
                        <label class="b-checkbox checkbox is-small">
                            <input type="checkbox" name="filters[work_status][]" value="{{ $status->id }}">
                            <span class="check is-info"></span>
                            <span class="control-label">{{ $status->ru_name }}</span>
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="column is-4">
            <label class="label" for="filters[work_type]">Работа</label>
            <div class="field">
                @foreach ($workTypes as $work)
                    <div class="control">
                        <label class="b-checkbox checkbox is-small">
                            <input type="checkbox" name="filters[work_type][]" value="{{ $work->id }}">
                            <span class="check is-info"></span>
                            <span class="control-label">{{ $work->ru_name }}</span>
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="column is-4">
            <label class="label">Стаж работы</label>
            <div class="field">
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[seniority][]" value="<1">
                        <span class="check is-info"></span>
                        <span class="control-label">До 1 года</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[seniority][]" value=">=1|<3">
                        <span class="check is-info"></span>
                        <span class="control-label">От 1 до 3 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[seniority][]" value=">=3|<7">
                        <span class="check is-info"></span>
                        <span class="control-label">От 3 до 7 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox" name="filters[seniority][]" value=">=7|<15">
                        <span class="check is-info"></span>
                        <span class="control-label">От 7 до 15 лет</span>
                    </label>
                </div>
                <div class="control">
                    <label class="b-checkbox checkbox is-small">
                        <input type="checkbox">
                        <span class="check is-info"></span>
                        <span class="control-label">Произвольный период</span>
                    </label>
                    <div class="field is-grouped">
                        <div class="control">
                            <input class="input is-small" type="number" min="1" max="50" name="filters[seniority][other][from]" placeholder="От">
                        </div>
                        <div class="control">
                            <input class="input is-small" type="number" min="1" max="50" name="filters[seniority][other][to]" placeholder="До">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='container has-text-centered'>
        <div class='columns is-mobile is-centered'>
            <div class='column is-12'>
                <div>
                    <h1 class='title'>Информация о пользователях</h1>
                    <hr>
                </div>
                <table class='table'>
                    <thead>
                        <tr>
                            <th>ФИО</th>
                            <th>ИИН</th>
                            <th>Категория</th>
                            <th>Возраст</th>
                            <th>Контактный телефон</th>
                            <th>Семейное положение</th>
                            <th>Адрес проживания</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    @include('users.table', ['users' => $users])
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
