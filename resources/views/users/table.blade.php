<tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user->fio }}</td>
            <td>{{ $user->iin }}</td>
            <td>
                @foreach($user->user_categories as $category)
                    {{ $category->ru_name }} <br />
                @endforeach
            </td>
            <td>{{ $user->age }}</td>
            <td>{{ $user->mobile_phone }}</td>
            <td>{{ $user->marital_status->ru_name }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->work_status->ru_name }}</td>
            <td><a href="/users/{{ $user->id }}" class="button is-success">Карточка пользователя</a></td>
        </tr>
    @endforeach
</tbody>