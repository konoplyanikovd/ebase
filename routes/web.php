<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UsersController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', HomeController::class);
Route::any('/users', [UsersController::class, 'index']);
Route::get('/users/:id', [UsersController::class, 'show'])->where('id', '[0-9]');
Route::post('/users/:id', [UsersController::class, 'edit'])->where('id', '[0-9]');
